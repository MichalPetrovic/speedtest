var imageAddr = "test.jpg";
var downloadSize = 18711753; //bytes

function ShowProgressMessage(msg) {
	if (console) {
		if (typeof msg == "string") {
			console.log(msg);
		} else {
			for (var i = 0; i < msg.length; i++) {
				console.log(msg[i]);
			}
		}
	}

	var oProgress = document.getElementById("progress");
	if (oProgress) {
		var actualHTML = (typeof msg == "string") ? msg : msg.join("<br />");
		oProgress.innerHTML = actualHTML;
	}
}

function InitiateSpeedDetection() {
	ShowProgressMessage("Meriam rýchlosť, prosím počkajte...");
	window.setTimeout(MeasureConnectionSpeed, 1);
};

if (window.addEventListener) {
	window.addEventListener('load', InitiateSpeedDetection, false);
} else if (window.attachEvent) {
	window.attachEvent('onload', InitiateSpeedDetection);
}

function MeasureConnectionSpeed() {
	var startTime, endTime;
	var download = new Image();
	download.onload = function () {
		endTime = (new Date()).getTime();
		showResults();
	}

	download.onerror = function (err, msg) {
		ShowProgressMessage("Chyba!");
	}

	startTime = (new Date()).getTime();
	var cacheBuster = "?nnn=" + startTime;
	download.src = imageAddr + cacheBuster;

	function showResults() {
		var duration = (endTime - startTime) / 1000;
		var bitsLoaded = downloadSize * 8;
		var speedBps = (bitsLoaded / duration).toFixed(2);
		var speedKbps = (speedBps / 1024).toFixed(2);
		//var speedMbps = (speedKbps / 1024).toFixed(2);
		var speedMbps = (downloadSize * 8 / duration / 1024 / 1024).toFixed(4);
		var speedMBs = (speedMbps * 0.125).toFixed(2);
		ShowProgressMessage([
            "Vaša rýchlosť internetu je:",
            "~" + speedMbps + " Mbps",
            "~" + speedMBs + "MB/s"
        ]);
	}
}
